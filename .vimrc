" Ref: https://github.com/junegunn/vim-plug#installation
" https://github.com/junegunn/vim-plug#usage
" https://github.com/junegunn/vim-plug#example
call plug#begin('~/.vim/plugged')
  " Plug 'https://github.com/rakr/vim-one.git'
  Plug 'sonph/onehalf', { 'rtp': 'vim' }
  " Plug 'dracula/vim', { 'as': 'dracula' }
  Plug 'vim-airline/vim-airline' " Colorful statusline 
  Plug 'tpope/vim-sensible' " a basic starting point vim config
  Plug 'tpope/vim-surround' " enhance quoting/parenthezing
  Plug 'tpope/vim-commentary' " allow commenting by file type
  Plug 'mileszs/ack.vim' " Searching across file
  " Plug 'sheerun/vim-polyglot' " Language pack for syntax highlighting
  " Plug 'dense-analysis/ale' " For linting, completetion etc
call plug#end()

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.177 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

let mapleader = "\<Space>" " Use the space key as leader. 
let g:airline_theme='onehalfdark'
colorscheme onehalfdark
" colorscheme one " Atom's color scheme
" colorscheme dracula " Dracula theme
set background=dark

" Netrw config - https://shapeshed.com/vim-netrw/
let g:netrw_banner = 0 " Removes the banner
let g:netrw_liststyle = 3 " Selects tree view
let g:netrw_browse_split = 3 " To open files in prev Tab

" Ack.vim init - to use silver searcher t
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" https://github.com/dense-analysis/ale/tree/master/ale_linters
" Define linter for current buffer
" let b:ale_linters ={ 'javascript': [ 'eslint' ], 'ruby': [ 'rubocop' ] }
" let g:ale_completion_enabled = 1 " Enable ale completion globally
" set omnifunc=ale#completion#OmniFunc

syntax on
filetype indent plugin on " http://vimdoc.sourceforge.net/htmldoc/filetype.html#:filetype-overview
" highlight Pmenu ctermbg=243 " https://vim.fandom.com/wiki/Xterm256_color_names_for_console_Vim?file=Xterm-color-table.png
" highlight PmenuSel ctermbg=white
set tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab
set relativenumber number
" Using System clipboard as default
" https://stackoverflow.com/a/30691754
set clipboard^=unnamed,unnamedplus
set nocompatible " Don't maintain compatibilty with Vi.
set scrolloff=4  " Keep at least 4 lines below cursor
set backspace=indent,eol,start  " Sane backspace behavior
set mouse=a " Enable mouse
set hls ic incsearch " set highlighted search, ignore case and show first match as we type
set wildignore+=$PWD/node_modules/** " to ignore when file name completion is used
set list " Show trailing whitespace
set noswapfile nobackup " Don't create a swap file and don't save backup

" Disable Ctrl-Z
nnoremap <c-z> <NOP>
" dd just deletes the line and doesn't save to register
nnoremap d "_d
vnoremap d "_d
" Move to the first char on the line
nmap 0 ^
" Map Ctrl-s to write the file
nmap <C-s> :w<cr>
imap <C-s> <esc>:w<cr>
" Map Ctrl-q to quit - only from normal mode
nmap <C-q> :q<cr>
" Keymap for bracket/parenthesis completion
" https://coderwall.com/p/h1dv0g/completion-for-parentheses-braces-and-brackets-in-vim
inoremap { {}<left>
inoremap {{ {
inoremap {} {}
inoremap [ []<left>
inoremap [[ [
inoremap [] []
inoremap ( ()<left>
inoremap (( (
inoremap () ()
inoremap " ""<left>
inoremap "" ""
inoremap ' ''<left>
inoremap '' ''
inoremap < <><left>
inoremap <> <>
inoremap << < 

" Command aliases for typoed commands (accidentally holding shift too long)
command! Q q " Bind :Q to :q
command! Qall qall
command! QA qall
command! Qow only " To quit all other windows
command! Qot tabonly " To quit all other tabs
" Ref: https://github.com/changemewtf/no_plugins/blob/master/no_plugins.vim -
" anything after ! treated as a shell command.
" * Requires ctags utility on the system - https://github.com/universal-ctags/ctags
command! MakeTags !ctags -R --exclude=node_modules .
command! MakeTagsForNodePkg !ctags -R --exclude=node_modules --exclude=lib .
" Commands to set path var to enable recursive search specific to a project
" type
command! RailsPath set path=$PWD,$PWD/app**,$PWD/spec**,$PWD/config**,$PWD/db**,$PWD/lib**
command! NodePath set path=$PWD,$PWD/src**,$PWD/test**,$PWD/translations**

" Bind `q` to close the buffer for help files 
" BufRead, BufWrite, BufEnter, VimEnter are some of the events
" One example of using Filetype is for file specific indentation
autocmd Filetype help nnoremap <buffer> q :q<CR>
autocmd Filetype java setlocal tabstop=4 softtabstop=4 shiftwidth=4 

" AUTOCOMPLETE: Ref: https://www.youtube.com/watch?v=XA2WjJbmmoM&list=WL&index=37
" The good stuff is documented in |ins-completion|
" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option

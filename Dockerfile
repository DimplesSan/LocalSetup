FROM alpine

WORKDIR /root/Workspace
# To persist, the cached dependencies
VOLUME /root/dependencies

# # Install runtimes needed - java, mvn, rbenv, ruby, nvm, node,
# Need to install imagick6 as latest is not compatible with rmaginck
# Ref: https://github.com/rmagick/rmagick/issues/834#issuecomment-590075500
RUN apk add --update --no-cache git bash\
    build-base readline-dev openssl-dev zlib-dev linux-headers libffi-dev \
    imagemagick6 imagemagick6-c++ imagemagick6-dev imagemagick6-libs \
    maven openjdk8 openjdk11 \
    && rm -rf /var/cache/apk/*

# Set req'd env variables and Java8 as default java runtime
ENV JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk
ENV PATH=$JAVA_HOME/bin:$PATH
RUN export MAVEN_HOME="$(mvn -version | grep "Maven home:" | cut -d ':' -f 2 | tr -d ' ')" && echo "$MAVEN_HOME"
ENV MVN_LOCAL_REPOSITORY=/root/dependencies/maven
# RUN java -version
COPY settings.xml /root/Workspace
RUN rm "$($MAVEN_HOME/conf/settings.xml)" \
    && mv /root/Workspace/settings.xml "$MAVEN_HOME/conf/"

# Set req'd env variables and ruby 2.6.6 as default ruby runtime
ENV RBENV_ROOT=/usr/local/rbenv
ENV PATH=$RBENV_ROOT/shims/:$RBENV_ROOT/bin:$PATH
RUN git clone https://github.com/rbenv/rbenv.git $RBENV_ROOT \
    && git clone https://github.com/rbenv/ruby-build.git $RBENV_ROOT/plugins/ruby-build \
    && $RBENV_ROOT/plugins/ruby-build/install.sh
# RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh
RUN echo 'eval "$(rbenv init -)"' >> /root/.bash_profile
RUN rbenv install 2.6.6 && rbenv global 2.6.6
RUN rbenv install 2.7.1

# We need to build code in container and persist the data cached dependencies so that they aren't downloaded everytime
# the container is started.
# Ref: https://docs.docker.com/storage/
# Example command to mount external File system and a dedicated volume for docker to persist data
# docker run -it \
#   --mount type="bind",source=/Users/sk046742/Workspace,target=/root/Workspace,consistency=delegated \
#   --mount type="volume",source=dependencies,target="/root/dependencies",consistency=delegated \
#   <image_name>

# Expose ports for networking
# rails
EXPOSE 3000 3001 3002 3003 3004 3005
# rack-up
EXPOSE 9292
# jetty
EXPOSE 8080 9000
